FROM elixir:latest AS builder

RUN apt-get update
RUN apt-get install inotify-tools -y
RUN apt install nodejs -y
RUN apt install npm -y

RUN mix local.hex --force
RUN mix local.rebar --force

WORKDIR /app

COPY ./salesloft_api_integration /app

RUN mix deps.get
RUN mix deps.compile

RUN cd assets && npm install && node_modules/webpack/bin/webpack.js --mode development

CMD mix ecto.setup && \
    mix phx.server
