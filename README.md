# salesloft_api_integration

This is a demo project that integrates with the public SalesLoft API.

To run this project locally, you just have to do:
  - In salesloft_api_integration/salesloft_api_integration create a .env text file. It has to contain
    `SALESLOFT_API_KEY=<Your api key>`
  - docker-compose build
  - docker-compose up

## TODO's
 - Add email service for user registration
 - Implement check for email confirmation
