import * as React from 'react';
import { Link } from 'react-router-dom';

import Main from '../components/main';
import jaro from 'wink-jaro-distance'

export default class FetchDataPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { people: [], loading: true, showHideCount: false, showHideDups: false };

    // Get the data from our API.
    fetch('/api/people')
      .then(response => response.json())
      .then(data => {
        this.setState({ people: data, loading: false });
      });

    this.characterCount = this.characterCount.bind(this);
    this.renderPeopleTable = this.renderPeopleTable.bind(this);
    this.showHideState = this.showHideState.bind(this);
    this.showHideDupsState = this.showHideDupsState.bind(this);
  }

  showHideState() {
    this.setState({
      ...this.state,
      showHideCount: !this.state.showHideCount
    })
  }

  showHideDupsState() {
    this.setState({
      ...this.state,
      showHideDups: !this.state.showHideDups
    })
  }

  renderPeopleTable() {
    const people = this.state.people

    return (
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Job</th>
          </tr>
        </thead>
        <tbody>
          {people.map(person => (
            <tr>
              <td>{person.display_name}</td>
              <td>{person.email_address}</td>
              <td>{person.title}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }

  characterCount() {
    const people = this.state.people
    let email_count = {}
    let count_list = []

    people.map(({ email_address }) => {
      [...email_address].forEach(char => email_count[char] = (email_count[char] || 0) + 1)
    })
    Object.keys(email_count).map( key =>
      count_list.push([key, email_count[key]])
    )
    count_list = count_list.sort((ele1, ele2) => ele2[1] - ele1[1])

    return (
      <table>
        <thead>
          <tr>
            <th>Character</th>
            <th>Count</th>
          </tr>
        </thead>
        <tbody>
          {count_list.map(([char, times]) => (
            <tr>
              <td>{char}</td>
              <td>{times}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }

  possibleDuplicates() {
    const people = this.state.people
    let dups = {}

    people.map((person) => {
      people.map(({ email_address }) => {
        if(person.email_address != email_address) {
          const {similarity} = jaro(person.email_address, email_address)
          if(similarity > 0.89) {
            const new_value = dups[person.email_address] ? dups[person.email_address].push(email_address) : [email_address]
            dups[person.email_address] = new_value
          }
        }
      })
    })

    return (
      <table>
        <thead>
          <tr>
            <th>Email</th>
            <th>Possible duplicates</th>
          </tr>
        </thead>
        <tbody>
          {Object.keys(dups).map(key => (
            <tr>
              <td>{key}</td>
              <td>{dups[key]}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }

  render() {
    const content = this.state.loading ? (
      <p>
        <em>Loading...</em>
      </p>
    ) : (
      <React.Fragment>
        <button onClick={this.showHideState}>
          {this.state.showHideCount ? 'Hide' : 'Display'} character count
        </button>
        {this.state.showHideCount && this.characterCount()}
        <button onClick={this.showHideDupsState}>
          {this.state.showHideDups ? 'Hide' : 'Display'} possible duplicates
        </button>
        {this.state.showHideDups && this.possibleDuplicates()}
        {this.renderPeopleTable()}
      </React.Fragment>
    );

    return (
      <Main>
        <h1>People Data</h1>
        {content}
        <br />
        <br />
        <p>
          <Link to="/">Back to home</Link>
        </p>
      </Main>
    );
  }
}
