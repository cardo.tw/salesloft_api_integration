import * as React from 'react'
import { RouteComponentProps } from 'react-router-dom'
import Main from '../components/main'

const HomePage = () => <Main>I'm a HomePage</Main>

export default HomePage
