import * as React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import HomePage from './pages'
import FetchDataPage from './pages/people_data'

export default class Root extends React.Component {
  render() {
    return (
      <>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/people" component={FetchDataPage} />
          </Switch>
        </BrowserRouter>
      </>
    )
  }
}
