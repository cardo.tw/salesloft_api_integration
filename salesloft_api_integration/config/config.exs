# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :salesloft_api_integration,
  ecto_repos: [SalesloftApiIntegration.Repo]

# Configures the endpoint
config :salesloft_api_integration, SalesloftApiIntegrationWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "/x5NDBFFzYFkPoqqMDVintei5o3fCM9JSF4vTM8E8RKGgde0q6Q3bP4Of8Xhs/5X",
  render_errors: [view: SalesloftApiIntegrationWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: SalesloftApiIntegration.PubSub,
  live_view: [signing_salt: "2GN3sUzQ"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
