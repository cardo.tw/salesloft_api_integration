defmodule SalesloftApiIntegration.Repo do
  use Ecto.Repo,
    otp_app: :salesloft_api_integration,
    adapter: Ecto.Adapters.Postgres
end
