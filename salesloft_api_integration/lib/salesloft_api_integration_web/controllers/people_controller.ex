defmodule SalesloftApiIntegrationWeb.PeopleController do
  use SalesloftApiIntegrationWeb, :controller
  alias SalesloftApiIntegrationWeb.Salesloft

  def index(conn, _params) do
    json(conn, Salesloft.get_all_people())
  end
end
