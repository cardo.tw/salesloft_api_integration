defmodule SalesloftApiIntegrationWeb.PageController do
  use SalesloftApiIntegrationWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
