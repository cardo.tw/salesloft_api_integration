defmodule SalesloftApiIntegrationWeb.Salesloft do
  use HTTPoison.Base
  @expected_fields ~w(
    display_name
    email_address
    title
  )

  @moduledoc """
  Service to do API calls to salesloft's people endpoint
  """
  def process_request_headers(_), do: [
    "Content-type": "application/json",
    "Authorization": "Bearer #{System.get_env("SALESLOFT_API_KEY")}"
  ]

  def process_url(url), do: "https://api.salesloft.com/v2" <> url

  def process_response_body(body) do
    Jason.decode!(body)
  end

  @doc """
  Returns a list with all the available people data.
  To do this, makes multiples request until next page is null.
  """
  def get_all_people(next_page \\ 1, list \\ [])
  def get_all_people(nil, list), do: list
  def get_all_people(next_page , list) do
    params = %{include_paging_counts: true, page: next_page, per_page: 100}
    response = get!("/people.json", [], params: params).body
    next_page = response["metadata"]["paging"]["next_page"]
    response = Enum.map(response["data"], &Map.take(&1, @expected_fields))

    get_all_people(next_page, response ++ list)
  end
end
